'use strict';

angular.module('roadSenseApp')
  .controller('MapsCtrl', function ($scope) {

  	$scope.map = { center: { latitude: 55.869532, longitude: -4.249756 }, zoom: 12, pan: 10 };

  	// $scope.centre = {
  	// 	latitude: 55.869532,
  	// 	longitude:-4.249756
  	// };

  	// $scope.pan = true;
  	// $scope.zoom = 10;

  });
